let s=window.location.href;
let ar=s.split("?")
var user=ar[1].split("=")[1]
user=eval(user)
var base=document.getElementsByClassName("base")[0];
var multi_delete_trash=document.getElementById("multi_delete");
multi_delete_trash.style.visibility="hidden";
var tick_count=0;
// console.log(typeof val)
var url="https://jsonplaceholder.typicode.com";
fetchPostData();
async function fetchPostData(){
    res=await fetch(url+"/posts?userId="+user);
    let data = await res.json() 
    for(let i=0;i<data.length;i++) {
        let post_html=`<div class="card" id="${data[i].id}">
                    <input type="checkbox">
                    <i class="fa fa-trash fa-lg"></i>
                    <i class="fa fa-edit fa-lg"></i>
                    <div class="card-title">${data[i].title}</div>
                    <div class="card-body">${data[i].body}</div>
                    <div class="cmnts">
                    <span>Comments</span>
                    `
        let post_id=data[i].id;
        let no_of_recent_comments=3;
        let comments_data= await fetchCommentsData(post_id, no_of_recent_comments);
        let comments_html=``;
        for(let j=0; j<no_of_recent_comments; j++){
            comments_html+=`<section>
                                <div class="cmnt_name">${comments_data[j].name}</div>
                                <div>${comments_data[j].body}</div>
                                <div class="cmnt_email">${comments_data[j].email}</div>
                            </section>`
        }
    
        comments_html+=`</div></div>`
        base.innerHTML+=post_html+comments_html;
    }

    singleDelete();
    cardSelected();
    multiDelete();
}
    

async function fetchCommentsData(post_id,no_of_recent_comments){
    let comments=[];
    let res=await fetch(url+"/comments?postId="+post_id);
    res = await res.json();
    let required=res.length-no_of_recent_comments;
    for(let i=0;i<res.length;i++){
        if(i>=required){
            comments.push(res[i]);
        }
    }
    return comments;
}

function singleDelete(){
    let i_delete=document.querySelectorAll(".card>.fa-trash");
    for(let i=0;i<i_delete.length;i++){
        i_delete[i].onclick=()=>{
            if(confirm("post will be deleted")){
                i_delete[i].parentNode.remove();
            }
        }
    }
}
function multiDelete(){
    let multi_delete_trash=document.getElementById("multi_delete");
    multi_delete_trash.onclick=()=>{
        let checked_boxes=document.querySelectorAll("[type='checkbox']:checked");
        let n=checked_boxes.length;
        tick_count-=n;
        if(confirm(n+" selected posts , will be deleted")){
            for(let i=0;i<n;i++){
                checked_boxes[i].parentNode.remove();
            }
            multi_delete_trash.style.visibility="hidden";
        }
    }
}
function cardSelected(){
    let ch_boxes=document.querySelectorAll("[type='checkbox']");
    for(i of ch_boxes){
        i.addEventListener("click",mainTrash);
    }
}
function mainTrash(){
    let multi_delete_trash=document.getElementById("multi_delete");
    // console.log(multi_delete_trash.style.visibility);
    if(this.checked){
        tick_count++;
        this.parentElement.style.border="4px solid rgb(0, 140, 255)";
        multi_delete_trash.style.visibility="visible";
    }
    else
    {
        tick_count--;
        this.parentElement.style.border="4px solid transparent";
        // let ar=document.querySelectorAll("[type=checkbox]:checked");
        if(tick_count==0)
            multi_delete_trash.style.visibility="hidden";
    }
    
}
